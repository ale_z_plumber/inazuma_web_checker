import requests, os, schedule, time

def get_pages():
    pages = [
        ("www.inazuma.jp","Detectado cambio en la página oficial de Inazuma Eleven."),
        ("www.inazuma.jp/victory-road/","La página japonesa de Inazuma Eleven Victory Road ha sido actualizada."),
        ("www.inazuma.jp/victory-road/en/","La página en inglés de Inazuma Eleven Victory Road ha sido actualizada."),
        ("www.inazuma.jp/victory-road/en/movie/","Se han modificado los videos disponibles de IE Victory Road"),
        ("www.inazuma.jp/victory-road/en/beta-test/","Encontrados cambios en la página en inglés de la beta de IEVR"),
        ("www.inazuma.jp/victory-road/beta-test/","Encontrados cambios en la página japonesa de la beta de IEVR"),
        ("www.inazuma.jp/victory-road/es/beta-test/","Encontrados cambios en la página española de la beta de IEVR"),
        ("www.inazuma.jp/victory-road/en/beta-test/patchnote/","Se han actualizado las notas del parche en inglés de la beta de IEVR"),
        ("www.inazuma.jp/victory-road/beta-test/patchnote/","Se han actualizado las notas del parche en japonés de la beta de IEVR"),
        ("www.level5.co.jp/blog/","Nueva entrada en el blog de desarrollo de Level5."),
        ("www.level5.co.jp/blog/en/","Nueva entrada en el blog de desarrollo en inglés de Level5.")
    ]
    # Todo está en un try para que no pete si falla la petición
    try:
        for page in pages:
            file = f"/tmp/inazuma_web_checker/{page[0].replace('/','_')}"
            url = f"https://{page[0]}"
            response = requests.get(url)
            response.encoding = 'utf-8'

            if response.status_code == 200:
                if not os.path.isfile(file):
                    with open(f"{file}", "w", encoding='utf8') as page_file:
                        page_file.write(response.text.replace('\r\n', '\n'))
                else:
                    with open(f"{file}", "r", encoding='utf8') as file1:
                        old_file = file1.read()
                        new_file = response.text.replace('\r\n', '\n')

                        if old_file != new_file:
                            mensaje = f"{page[1]}\nPuedes acceder desde la siguiente url: {url}!"
                            telegram_message(mensaje)
                            os.remove(file)
                            #with open(f"{file}", "w", encoding='utf8') as page_file:
                            #    page_file.write(response.text.replace('\r\n', '\n'))
            else:
                print('Failed to retrieve the webpage. Status code:', response.status_code)
    except:
        print("Ha ocurrido algún error")

def telegram_message(message):
    token = os.environ.get("telegram_token")
    channel = os.environ.get("telegram_channel")
    url = f"https://api.telegram.org/bot{token}/sendMessage"
    data = {
        "chat_id": f"-{channel}",
        "text": f"{message}",
    }
    response = requests.post(url, json=data)
    return response

get_pages()
schedule.every(1).minutes.do(get_pages)
while True:
    schedule.run_pending()
    time.sleep(1)
