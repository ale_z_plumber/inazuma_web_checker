FROM python:3.9.19-slim-bullseye

WORKDIR /usr/src/app

COPY app.py .

RUN pip install --no-cache-dir requests schedule && mkdir /tmp/inazuma_web_checker/

CMD [ "python", "./app.py" ]