# Inazuma Web Checker
Aplicación creada por ale_z_plumber con el obetivo de monitorizar cambios en 
páginas web, concretamente de Inazuma Eleven.

## Requerimientos

El programa para funcionar requiere de recibir el token del bot de telegram
que usar y el id del canal donde publicar los avisos(sin el guión), como
variables de entorno. Aunque también puedes modificar las líneas donde recoge
las variables y hardcodearlas tú mismo.

Si quieres modificar las páginas que el programa trackea debes ir a la
definición de la variable `pages`, cada página se define mediante una tupla,
donde a la izquierda de la coma se añade la url (sin https://), y a la derecha
de la coma el mensaje personalizado que usará el programa al encontrar cambios
en esa web.

## Como ejecutarlo:

### Método 1, docker:
El método recomendado. Debes tener instalado docker engine de forma nativa
en Linux o docker desktop en Windows o Mac.
Si quieres ejecutar la aplicación las mismas URLs y mensajes de aviso,
simplemente crea un fichero `docker-compose.yaml` con el siguiente contenido:
```
services:
  ie_wc:
    image: alezplumber/inazuma_web_checker:1.2
    container_name: ie_wc
    restart: unless-stopped
    environment:
      telegram_token: token
      telegram_channel: id
    volumes:
      - /var/tmp/inazuma_web_checker/:/tmp/inazuma_web_checker/
```

Dentro modifica las variables `telegram_token` y `telegram_channel` con los valores
que necesites. Además asegúrate de que el volumen que se montará en el contenedor
apunta al lugar correcto en tu equipo. En sistemas Linux puedes dejarlo como está.
En windows o Mac modifica la ruta a la izquierda de los : para definir donde
guardar las web que obtiene el programa para que aunque caiga, al volver mantenga
el estado anterior de las web.
Entonces ejecuta el comando `docker compose up`.

En caso de que desees modificar las urls en primer lugar clona el repositorio:
`git clone git@gitlab.com:ale_z_plumber/inazuma_web_checker.git`

entra en el directorio con `cd inazuma_web_checker`

modifica las urls y mensajes en app.py como se explica arriba, una vez acabado
crea una nueva imagen local con el siguiente comando:
`docker build . -t nombre_imagen:version`
por ejejmplo: inazuma_web_checker:1.1

Ahora crea un fichero `docker-compose.yaml` como se especifica arriba pero
modificando la imagen al nombre_imagen:version que hayas seleccionado arriba.

Ahora inicialo con `docker compose up -d` y listo.

### Método 2, ejecución local:
Si usas un sistema linux solo debes definir las variables de entorno 
`telegram_token` y `telegram_channel`
`export telegram_token=token`
`export telegram_channel=id`
tener instalados los módulos schedule y requests de python
`pip install schedule requests`

y ejecutar el programa 
`python3 app.py`

En un sistema Windows deberás modificar el programa para que utilice rutas
acorde a un sistema windows, probablemente hardcodear las variables de entorno,
asegurarte de tener los módulos schedule y requests y entonces ejecutar el programa.
